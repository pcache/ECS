﻿
using System;
using System.Collections.Generic;
using ECS;
public class StressTest : ECSSystem
{
    [Inject(typeof(TransformComp))]
    private Match match;

    private bool create = true;
    private int cycles = 100000;

    public StressTest(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        if (create)
        {
            for (int i = 0; i < cycles; i++)
            {
                var e = this.entityManager.CreateEntity();
                e.AddComponent(TransformComp.typeid);
            }

            create = false;
        }
        else
        {
            for (int i = 0; i < match.Length; i++)
            {
                this.PostUpdate.DestroyEntity(match.Entities[i]);
            }
            create = true;
        }

    }
}
