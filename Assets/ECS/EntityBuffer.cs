﻿namespace ECS
{

    using System;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Used in EntityManager to store existing entities compactly and linearly, and optionally sorted by id
    /// </summary>
    public class EntityBuffer
    {
        public Entity[] array;
        public int Length;

        public EntityBuffer(int capacity)
        {
            array = new Entity[capacity];
        }

    }

}