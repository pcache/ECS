﻿namespace ECS
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// This object represents one ECS instance.
    /// It stores and manages entities, components and systems.
    /// You should call update on it manually.
    /// </summary>
    public class ECSRoot
    {
        private const int hundredK = 100000;
        private const int oneMil = 1000000;

        /*
         * Parameters below dictate how to expand internal arrays, allocation performance significantly depends on these,
         * low values can consume less memory but make large allocations more expensive.
         */

        /// <summary>
        /// How many components can go into pool before it expands by this number.
        /// </summary>
        public const int PoolBlockSize = hundredK;

        /// <summary>
        /// How many entities can go into pool before it expands by this number.
        /// </summary>
        public const int EntityBlockSize = hundredK;

        /// <summary>
        /// How many components will be pre allocated as soon as no free components are left.
        /// </summary>
        public const int PoolPreAllocationBatch = 100;

        /// <summary>
        /// The size of matcher internal entity array
        /// </summary>
        public const int MatcherBlockSize = hundredK;


        /// <summary>
        /// The size of entity internal component array.
        /// </summary>
        public const int EntityInternalComponentArrayBlockSize = 5;

        /// <summary>
        /// If enabled will run sort on free ids, gets expensive if you remove a lot of entities, very fast otherwise.
        /// </summary>
        public static bool SortIDS { get; set; }

        public EntityManager EntityManager { get; private set; }
        public SystemsController SystemsController { get; private set; }

        private bool _paused;
        public bool Paused
        {
            get
            {
                return _paused;
            }

            set
            {
                this.SystemsController.PauseSimulation(value);
                this._paused = value;
                Time.timeScale = value ? 0f : 1f;
            }
        }

        public ECSRoot()
        {
            this.EntityManager = new EntityManager();
            this.SystemsController = new SystemsController(this);
        }

        public void Update()
        {
            this.EntityManager.Update();
            this.SystemsController.Update();
        }

        public void OnDrawGizmos()
        {
            this.SystemsController.OnDrawGizmos();
        }

        public void OnGUI()
        {
            this.SystemsController.OnGUI();
        }
    }

}