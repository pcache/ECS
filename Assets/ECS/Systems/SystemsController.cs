﻿namespace ECS
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using ECS.Internal;
    using ECS.Systems;
    using UnityEngine;


    public class SystemsController
    {
        private ECSRoot root;

        /// <summary>
        /// List of all currently registered systems
        /// </summary>
        private List<Controller> systems = new List<Controller>();

        private static Dictionary<int, Matcher> existingMatchers = new Dictionary<int, Matcher>();

        private bool Paused;

        public void PauseSimulation(bool value)
        {
            Paused = value;
        }

        public SystemsController(ECSRoot root)
        {
            this.root = root;
        }

        public void AddSystem<T>() where T : ECSSystem
        {
            T sys = (T)Activator.CreateInstance(typeof(T), root);

            var type = typeof(T);

            Controller controller = new Controller(sys, this.root);
            this.systems.Add(controller);
        }

        public void Update()
        {
            float delta = Time.deltaTime;

            if (!Paused)
            {
                foreach (var s in this.systems)
                {
                    s.Update(delta);
                }
            }
        }

        internal void OnDrawGizmos()
        {
            float delta = Time.deltaTime;

            foreach (var s in this.systems)
            {
                s.OnDrawGizmos(delta);
            }
        }

        internal void OnGUI()
        {
            float delta = Time.deltaTime;

            foreach (var s in systems)
            {
                s.OnGUI(delta);
            }
        }

        internal void ClearSystems() => this.systems.Clear();

        /// <summary>
        /// Wrapper for a system that stores all necessary information and updates them.
        /// </summary>
        private class Controller
        {
            /// <summary>
            /// Target system
            /// </summary>
            private ECSSystem system;
            private ISystemCommands commands;
            private MatchInjector[] matchinjectors;

            public Controller(ECSSystem system, ECSRoot root)
            {
                this.system = system;
                this.commands = system.GetCommands();

                Type t = system.GetType();
                List<MatchInjector> injectorsList = new List<MatchInjector>();

                // Find inject targets
                var fields = t.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                foreach (var f in fields)
                {
                    var attr = (InjectAttribute)f.GetCustomAttribute(typeof(InjectAttribute));
                    if (attr != null)
                    {
                        // Inject component pools into system
                        if ((typeof(ComponentTrackerBase).IsAssignableFrom(f.FieldType)))
                        {
                            Type pooltype = f.FieldType.GetGenericArguments()[0];
                            f.SetValue(this.system, this.system.entityManager.GetPool(ECSTypeHandler.COMPONENT_TYPES_ID[pooltype]));
                        }

                        // Create match injectors
                        if ((typeof(Match).IsAssignableFrom(f.FieldType)))
                        {
                            MatchInjector injector = new MatchInjector(f, root, system, attr.Types);
                            injectorsList.Add(injector);
                        }

                    }
                }

                this.matchinjectors = injectorsList.ToArray();

                // Here we get all OnAddComp, OnRemoveComp methods from the system and register them into correspoding Pool.

                var interfaces = t.GetInterfaces();

                foreach (var item in interfaces)
                {
                    var interfaceType = typeof(IComponentAddedReceiver<>);
                    if (item.IsGenericType && item.GetGenericTypeDefinition() == interfaceType)
                    {
                        var argtype = item.GetGenericArguments()[0];
                        int typeid = ECSTypeHandler.COMPONENT_TYPES_ID[argtype];
                        var pools = root.EntityManager.GetPools();
                        pools[typeid].RegisterSystemCompAdded(system);
                    }

                    interfaceType = typeof(IComponentRemovedReceiver<>);
                    if (item.IsGenericType && item.GetGenericTypeDefinition() == interfaceType)
                    {
                        var argtype = item.GetGenericArguments()[0];
                        int typeid = ECSTypeHandler.COMPONENT_TYPES_ID[argtype];
                        var pools = root.EntityManager.GetPools();
                        pools[typeid].RegisterSystemCompRemoved(system);
                    }
                }

                system.Awake();

            }

            /// <summary>
            /// Handles a single matcher, injects the results into the system Match
            /// into System group.
            /// </summary>
            private class MatchInjector
            {

                private Match targetMatch;
                private Matcher matcher;
                private ECSSystem system;

                private int[] typeids;

                public MatchInjector(FieldInfo field, ECSRoot root, ECSSystem system, Type[] types)
                {
                    this.system = system;

                    // Create matcher based on Comp types
                    this.typeids = new int[types.Length];
                    for (int i = 0; i < this.typeids.Length; i++)
                    {
                        this.typeids[i] = ECSTypeHandler.COMPONENT_TYPES_ID[types[i]];
                    }

                    this.typeids.OrderBy(x => x).ToArray();
                    int hash = GetHash(this.typeids);

                    SystemsController.existingMatchers.TryGetValue(hash, out this.matcher);
                    if (this.matcher == null)
                    {
                        this.matcher = new Matcher(system.entityManager, this.typeids);
                        SystemsController.existingMatchers.Add(hash, this.matcher);
                    }

                    // Create new instance of Match
                    this.targetMatch = (Match)Activator.CreateInstance(typeof(Match), this.matcher.MatcherID, root);
                    field.SetValue(system, this.targetMatch);



                    // Register matcher in pools
                    for (int i = 0; i < this.typeids.Length; i++)
                    {
                        ComponentTrackerBase pool = system.entityManager.GetPool(this.typeids[i]);
                        pool.RegisterMatcher(this.matcher);
                    }
                }

                public void Inject()
                {
                    this.matcher.Prepare();
                    this.targetMatch.Length = this.matcher.Length;
                    this.targetMatch.Entities = this.matcher.MatchingEntityIDs;
                }

                private int GetHash(int[] arr)
                {
                    int hc = arr.Length;
                    for (int i = 0; i < arr.Length; ++i)
                    {
                        hc = unchecked(hc * 314159 + arr[i]);
                    }
                    return hc;
                }
            }

            public void Update(float delta)
            {
                foreach (var injector in this.matchinjectors)
                {
                    injector.Inject();
                }
                this.system.Update(delta);
                this.commands.ExecuteCommands();
            }

            public void OnDrawGizmos(float delta)
            {
                this.system.OnDrawGizmos(delta);
            }

            public void OnGUI(float delta)
            {
                this.system.OnGUI(delta);
            }

            private bool IsAssignableToGenericType(Type givenType, Type genericType)
            {
                var interfaceTypes = givenType.GetInterfaces();

                foreach (var it in interfaceTypes)
                {
                    if (it.IsGenericType && it.GetGenericTypeDefinition() == genericType)
                    {
                        return true;
                    }
                }

                if (givenType.IsGenericType && givenType.GetGenericTypeDefinition() == genericType)
                {
                    return true;
                }

                Type baseType = givenType.BaseType;
                if (baseType == null)
                {
                    return false;
                }

                return IsAssignableToGenericType(baseType, genericType);
            }

        }


    }
}

