﻿/*using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public static class InspectorWindow {

    private static int FIELDCOUNT = 0;
    private static int FIELDHEIGHT = 20;
    private static int FIELDPADDING = 2;
    private static float PADDING = 10f;
    private static bool changed = false;
    private static bool browserForcedChanged = false;


    public static bool GenericInspector(object obj, Rect rect) {

        Rect fullrect = rect;
        Rect paddedrect = new Rect(rect.x + PADDING, rect.y + PADDING, rect.width - (PADDING * 2), rect.height - (PADDING * 2));
        GUI.Box(fullrect, "");

        if (obj == null)
            return false;

        FIELDCOUNT = 0;
        changed = false;
        
        DrawFields(obj, paddedrect);

        if(browserForcedChanged) {
            browserForcedChanged = false;
            return true;
        }

        return changed;
    }

    private static void DrawFields(object obj, Rect rect) {

        Type t = obj.GetType();
        //FieldInfo[] fields = t.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        PropertyInfo[] props = t.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

        Rect headfill = GetLineFillRect(rect);
        headfill.height *= 2;
        //UIManager.DrawColor(headfill, new Color(0.1f, 0.1f, 0.1f));

        string header = typeof(Def).IsAssignableFrom(t) ? ((Def)obj).name + "<color=grey>::" + t.Name + "</color>" : t.Name;

        GUI.Label(new Rect(headfill.getWithPadding(5)), header, UIManager.labelBoldStyle);
        Event e = Event.current;

        FIELDCOUNT += 2;

        //GUI.Box(rect.getWithPadding(2), "");

        foreach (var prop in props) {
            Type pt = prop.PropertyType;

            bool hide = prop.GetCustomAttributes(typeof(GameInspectorHideAttribute), false).Length > 0;
            if (hide)
                continue;

            bool readOnly = prop.GetCustomAttributes(typeof(GameInspectorReadOnlyAttribute), false).Length > 0;
            bool embed = prop.GetCustomAttributes(typeof(GameInspectorEmbedAttribute), false).Length > 0;

            object value = prop.GetValue(obj, null);

            if (!embed) {
                //if (FIELDCOUNT % 2 == 0)
                //    UIManager.DrawColor(GetLineFillRect(rect), new Color(0.3f, 0.3f, 0.3f));
                GUI.Label(GetLabelRect(rect), prop.Name);
            }

            //if (value != null) {

            if (pt == typeof(int)) {
                int init = (int)value;
                int result = init;

                var r = GetValueRect(rect);
                GUI.Label(r, init.ToString());
                int size = FIELDHEIGHT - 2;
                if (!readOnly) {

                    if (GUI.Button(new Rect(r.x + r.width - size - 5, r.y, size, size), "+")) {
                        if (e.control && e.shift) {
                            result += 1000;
                        }
                        else
                        if (e.control) {
                            result += 100;
                        }
                        else
                            if (e.shift) {
                            result += 10;
                        }
                        else
                            result += 1;
                    }

                    if (GUI.Button(new Rect(r.x + r.width - size * 2 - 10, r.y, size, size), "-")) {
                        if (e.control && e.shift) {
                            result -= 1000;
                        }
                        else
                        if (e.control) {
                            result -= 100;
                        }
                        else
                            if (e.shift) {
                            result -= 10;
                        }
                        else
                            result -= 1;
                    }

                    //Int32.TryParse(GUI.TextField(GetValueRect(rect), ), out result);
                }

                if (result != init) {
                    prop.SetValue(obj, result, null);
                    changed = true;
                }

            }
            else if (pt == typeof(string)) {
                string init = (string)value;
                string result = init;

                if (!readOnly) {
                    result = GUI.TextField(GetValueRect(rect), init.ToString());
                }

                if (result != init) {
                    prop.SetValue(obj, result, null);
                    changed = true;
                }
            }
            else if (pt == typeof(bool)) {
                bool init = (bool)value;
                bool result = init;

                if (!readOnly) {
                    Rect r = GetValueRect(rect);
                    result = GUI.Toggle(new Rect(r.x + r.width - FIELDHEIGHT, r.y, FIELDHEIGHT, FIELDHEIGHT), init, "");
                }

                if (result != init) {
                    prop.SetValue(obj, result, null);
                    changed = true;
                }
            }
            else if (pt == typeof(Image)) {
                Image init = (Image)value;

                if (!readOnly) {
                    Rect r = GetValueRect(rect);
                    Rect imgRect = new Rect(r.x, r.y, FIELDHEIGHT * 4, FIELDHEIGHT * 4);
                    Texture2D tex = null;
                    if (init != null) {
                        if (init.meta.IsSprite && init.sprite != null)
                            tex = init.sprite.texture;
                        else if (init.texture != null) {
                            tex = init.texture;
                        }
                    }
                    if (tex != null) {
                        if (GUI.Button(imgRect, tex)) {
                            EditorState.browser.ACTIVE = true;
                            EditorState.browser.Open(x => {
                                prop.SetValue(obj, x);
                                InspectorWindow.browserForcedChanged = true;
                            }, EditorBrowserWindow.Mode.Image);
                        }
                    }
                    else {
                        if(GUI.Button(imgRect, RenderingSystem.NoSprite.texture)) {
                            EditorState.browser.ACTIVE = true;
                            EditorState.browser.Open(x => {
                                prop.SetValue(obj, x);
                                InspectorWindow.browserForcedChanged = true;
                            }, EditorBrowserWindow.Mode.Image);
                        }
                    }
                    FIELDCOUNT += 3;

                }
            }
            else if (embed) {
                DrawFields(value, rect);
            }

            if (value != null && readOnly) {
                GUI.Label(GetValueRect(rect), value.ToString());
            }

            FIELDCOUNT++;

        }

    }

    /// <summary>
    /// Call OnGUI once, before this
    /// returns true if changes were detected
    /// </summary>
    /// <param name="obj"></param>
    //private bool InspectImage(Image obj) {
    //
    //    LABEL_FIELD("Name", obj.name);
    //    LABEL_FIELD("Width", obj.texture.width.ToString());
    //    LABEL_FIELD("Height", obj.texture.height.ToString());
    //
    //    if (obj.meta != null) {
    //        if (BOOL_FIELD(ref obj.meta.Tiled, "Tiled", false))
    //            changed = true;
    //    }
    //
    //
    //    return changed;
    //}
    //
    ////private void LABEL_FIELD(string name, string value) {
    ////    GUI.Label(GetLabelRect(), name);
    ////    GUI.Label(GetValueRect(), value);
    ////    FIELDCOUNT++;
    ////}
    ////
    //
    //private bool INT_FIELD(ref int val, string name, bool readOnly) {
    //    int init = val;
    //
    //    GUI.Label(GetLabelRect(), name);
    //    if (!readOnly)
    //        Int32.TryParse(GUI.TextField(GetValueRect(), val.ToString()), out val);
    //    FIELDCOUNT++;
    //
    //    if (val != init)
    //        return true;
    //    else
    //        return false;
    //}
    //
    //private bool STR_FIELD(ref string val, string name, bool readOnly) {
    //    string init = val;
    //
    //    GUI.Label(GetLabelRect(), name);
    //    if (val == null)
    //        val = "";
    //    if (!readOnly)
    //        val = GUI.TextField(GetValueRect(), val.ToString());
    //
    //    FIELDCOUNT++;
    //
    //    if (val != init)
    //        return true;
    //    else
    //        return false;
    //}
    //
    //private bool BOOL_FIELD(ref bool val, string name, bool readOnly) {
    //    bool init = val;
    //
    //    GUI.Label(GetLabelRect(), name);
    //
    //    if (!readOnly)
    //        val = GUI.Toggle(GetValueRect(), val, "");
    //    FIELDCOUNT++;
    //
    //    if (val != init)
    //        return true;
    //    else
    //        return false;
    //}

    private static Rect GetLineFillRect(Rect rect) {
        return new Rect(rect.x, rect.y + (FIELDPADDING + FIELDHEIGHT) * FIELDCOUNT, rect.width, FIELDHEIGHT);
    }

    private static Rect GetSubObjectRect(Rect rect) {
        return new Rect(rect.x, rect.y + (FIELDPADDING + FIELDHEIGHT) * FIELDCOUNT, rect.width / 2, FIELDHEIGHT);
    }

    private static Rect GetLabelRect(Rect rect) {
        return new Rect(rect.x, rect.y + (FIELDPADDING + FIELDHEIGHT) * FIELDCOUNT, rect.width / 2, FIELDHEIGHT);
    }

    private static Rect GetValueRect(Rect rect) {
        return new Rect(rect.x + (rect.width / 2), rect.y + (FIELDPADDING + FIELDHEIGHT) * FIELDCOUNT, rect.width / 2, FIELDHEIGHT);
    }
}
*/