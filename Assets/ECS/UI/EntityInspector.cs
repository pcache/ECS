﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using ECS;
using System.Reflection;

public class EntityInspector
{

    private static int FIELDCOUNT = 0;
    private static int FIELDHEIGHT = 20;
    private static int FIELDPADDING = 2;
    private static float PADDING = 10f;
    private static int currentposY = 0;

    private static bool changed = false;
    private ECSRoot root;
    private List<Comp> buffer = new List<Comp>();
    private List<FieldInfo[]> fieldsBuffer = new List<FieldInfo[]>();
    private Vector2 scroll;
    public bool Inspect(int entityID, Rect rect, ECSRoot root)
    {
        this.root = root;

        Rect fullrect = rect;
        Rect paddedrect = new Rect(rect.x + PADDING, rect.y + PADDING, rect.width - (PADDING * 2), rect.height - (PADDING * 2));
        GUI.Box(fullrect, "");

        if (!root.EntityManager.Exists(entityID))
        {
            return false;
        }

        Entity e = root.EntityManager.GetEntity(entityID);

        FIELDCOUNT = 0;
        changed = false;

        GUI.Label(GetLabelRect(paddedrect, FIELDCOUNT), e.Name + ":" + e.ID);
        FIELDCOUNT++;
        root.EntityManager.GetAllComponents(e, buffer);
        currentposY = 0;

        FillFieldBuffer();

        Rect scrollviewStart = GetLabelRect(paddedrect, FIELDCOUNT);
        Rect position = new Rect(scrollviewStart.x, scrollviewStart.y, paddedrect.width, paddedrect.height - 40);
        int scrollHeight = CalcTotalComponentsHeight();
        Rect viewRect = new Rect(position.x, position.y, position.width - 20, scrollHeight);
        scroll = GUI.BeginScrollView(position, scroll, viewRect);

        foreach (var comp in buffer)
        {
            var fields = GetFields(comp);
            int count = fields.Length;
            DrawComponent(comp, fields, e, GetCompRect(viewRect, count));
            currentposY += count * FIELDHEIGHT;
            FIELDCOUNT++;
        }

        GUI.EndScrollView();

        return changed;
    }

    private void FillFieldBuffer()
    {
        fieldsBuffer.Clear();

        for (int i = 0; i < buffer.Count; i++)
        {
            fieldsBuffer.Add(GetFields(buffer[i]));
        }
    }

    private int CalcTotalComponentsHeight()
    {
        int height = 0;

        foreach (var fieldarray in fieldsBuffer)
        {
            foreach (var field in fieldarray)
            {
                height += FIELDHEIGHT;
            }

            height += FIELDHEIGHT;
        }

        return height;
    }

    private FieldInfo[] GetFields(Comp comp)
    {
        return comp.GetType().GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
    }

    private void DrawComponent(Comp comp, FieldInfo[] fields, Entity e, Rect r)
    {
        Type t = comp.GetType();
        int fieldCount = 0;
        GUI.Box(r, "");

        Rect labelRect = GetLabelRect(r, fieldCount);
        GUI.Label(labelRect, t.Name + " | id: " + comp.Typeid);
        if (GUI.Button(new Rect(r.x + r.width - FIELDHEIGHT, labelRect.y, FIELDHEIGHT, FIELDHEIGHT), "X"))
        {
            e.DestroyComponent(comp.Typeid);
            return;
        }

        fieldCount++;

        foreach (var f in fields)
        {
            if (f.Name == "EntityID")
            {
                continue;
            }

            GUI.Label(GetLabelRect(r, fieldCount), f.Name);
            GUI.Label(GetValueRect(r, fieldCount), f.GetValue(comp).ToString());
            fieldCount++;
        }
    }

    private static Rect GetLineFillRect(Rect rect) => new Rect(rect.x, rect.y + (FIELDPADDING + FIELDHEIGHT) * FIELDCOUNT, rect.width, FIELDHEIGHT);

    private static Rect GetSubObjectRect(Rect rect) => new Rect(rect.x, rect.y + (FIELDPADDING + FIELDHEIGHT) * FIELDCOUNT, rect.width / 2, FIELDHEIGHT);

    private static Rect GetLabelRect(Rect rect, int fieldCount) => new Rect(rect.x + 3, rect.y + (FIELDPADDING + FIELDHEIGHT) * fieldCount + 2, rect.width / 2, FIELDHEIGHT);

    private static Rect GetCompRect(Rect rect, int compfieldCount)
    {
        return new Rect(rect.x, rect.y + (FIELDCOUNT * FIELDHEIGHT) + currentposY, rect.width, compfieldCount * FIELDHEIGHT + PADDING);
    }

    private static Rect GetValueRect(Rect rect, int fieldCount) => new Rect(rect.x + (rect.width / 2), rect.y + (FIELDPADDING + FIELDHEIGHT) * fieldCount, rect.width / 2, FIELDHEIGHT);
}
