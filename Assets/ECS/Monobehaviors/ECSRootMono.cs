﻿namespace ECS
{
    // Alexander Antonov, www.pointcache.com all rights reserved. 

    using System;
    using System.Collections.Generic;
    using ECS;
    using UnityEngine;

    public class ECSRootMono : MonoBehaviour
    {

        private ECSDebugGUI debugGUI;

        [SerializeField]
        private bool drawUI = false;

        public ECSRoot ECS { get; private set; }

        protected virtual void Awake()
        {
            this.ECS = new ECSRoot();
        }

        // Use this for initialization
        protected virtual void Start()
        {
            this.debugGUI = new GameObject("DebugGUI").AddComponent<ECSDebugGUI>();
            this.debugGUI.SetRoot(this.ECS);
            this.debugGUI.gameObject.SetActive(this.drawUI);
        }

        protected virtual void Update()
        {
            this.ECS.Update();

            if (this.ECS.EntityManager.Count < 1000)
            {
                this.debugGUI.gameObject.SetActive(this.drawUI);
            }
            else
            {
                this.debugGUI.gameObject.SetActive(false);
            }
        }

        protected virtual void OnDrawGizmos()
        {
            if (ECS != null)
            {
                this.ECS.OnDrawGizmos();
            }
        }

        protected virtual void OnGUI()
        {
            if (ECS != null)
            {
                this.ECS.OnGUI();
            }
        }
    }

}