﻿namespace ECS
{
    // Alexander Antonov, www.pointcache.com all rights reserved. 

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class GameObjectPool<T> where T : GameObjectTemplate, new()
    {
        private T[] free;
        private int count;
        private const int blockSize = 1000;
        private const int batchSize = 10;
        private Transform root;

        private ECSObjectActivator.Activator.ObjectActivator activator;

        //ConditionalWeakTable is available in .NET 4.0+
        //if you use an older .NET, you have to create your own CWT implementation (good luck with that!)



        public GameObjectPool()
        {
            this.root = new GameObject(nameof(T)).transform;
            this.root.hideFlags = HideFlags.HideInHierarchy;

            this.free = new T[0];
            var constructors = typeof(T).GetType().GetConstructors(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            var ctor = constructors.First(x => x.GetParameters().Length == 0);
            activator = new ECSObjectActivator.Activator(ctor).activator;
        }

        public T Get(Entity entity)
        {
            // ConstructMore
            if (count == 0)
            {
                if (free.Length < batchSize)
                {
                    // Expand
                    T[] arr = new T[free.Length + blockSize];
                    Array.Copy(free, arr, count);
                    free = arr;
                }

                for (int i = 0; i < batchSize; i++)
                {
                    T newtemplate = new T();
                    newtemplate.gameObject = newtemplate.Construct();
                    newtemplate.transform = newtemplate.gameObject.transform;
                    newtemplate.transform.SetParent(root);
                    free[i] = newtemplate;
                    count++;
                }
            }

            T template = free[count - 1];
            count--;
            template.transform.SetParent(null);
            template.collider.EntityID = entity.ID;

            return template;


        }

        public void Release(T template)
        {
            // Expand
            if (count + 1 > free.Length)
            {
                T[] arr = new T[free.Length + blockSize];
                Array.Copy(free, arr, count);
                free = arr;
            }
            template.transform.SetParent(root);
            template.collider.EntityID = -1;
            template.Reset();

            free[count] = template;
            count++;
        }



    }

}