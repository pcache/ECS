﻿namespace ECS
{
    // Alexander Antonov, www.pointcache.com all rights reserved. 

    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class GameObjectTemplate
    {
        public GameObject gameObject;
        public Collider2D collider;
        public Transform transform;
        public Entity entity;

        public abstract GameObject Construct();
        public abstract void Reset();
    }

}