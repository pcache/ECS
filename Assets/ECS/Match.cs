﻿namespace ECS {

    using System;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Stores array with entities that matches provided component types by InjectAttribute
    /// </summary>
    public class Match {

        public Entity this[int index]
        {
            get
            {
                return manager.GetEntity(Entities[index]);
            }
        }

        public int Length;
        public int[] Entities;
        public readonly int MatcherID;

        public Match(int matcherID, ECSRoot root)
        {
            this.MatcherID = matcherID;
            this.root = root;
            this.manager = root.EntityManager;
        }

        private ECSRoot root;
        private EntityManager manager;
    }

}