﻿namespace pointcache.Utility {
    using UnityEngine;
    using System;
    using System.Collections.Generic;


    public static class ByteTools {

        public static byte SetBit(this byte self, int bit, bool value) {

            byte mask = (byte)(1 << bit);

            return (byte)(value ? (self | mask) : (self & ~mask));

        }

        public static byte FlipBit(this byte self, int bit, bool value) {

            byte mask = (byte)(1 << bit);

            return self ^= mask;

        }

        public static bool GetBit(this byte self, int bit) {

            byte mask = (byte)(1 << bit);

            return (self & mask) != 0;

        }

        public static byte[] SetBit(this byte[] self, int index, bool value) {
            int byteIndex = index / 8;
            int bitIndex = index % 8;
            byte mask = (byte)(1 << bitIndex);

            self[byteIndex] = (byte)(value ? (self[byteIndex] | mask) : (self[byteIndex] & ~mask));
            return self;
        }

        public static byte[] FlipBit(this byte[] self, int index) {
            int byteIndex = index / 8;
            int bitIndex = index % 8;
            byte mask = (byte)(1 << bitIndex);

            self[byteIndex] ^= mask;
            return self;
        }

        public static bool GetBit(this byte[] self, int index, bool value) {
            int byteIndex = index / 8;
            int bitIndex = index % 8;
            byte mask = (byte)(1 << bitIndex);

            return (self[byteIndex] & mask) != 0;
        }
    }

}