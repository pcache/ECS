﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IMGUIUtilities {

    public static Rect GetWorldCenterRect() {
        var v2 = GUIUtility.ScreenToGUIPoint(Camera.main.WorldToScreenPoint(Vector3.zero));
        return new Rect(v2.x, Screen.height - v2.y, 0, 0);
    }

    public static Rect getWithPadding(this Rect rect, int padding) {
        return new Rect(rect.x + padding, rect.y + padding, rect.width - padding - padding, rect.height - padding - padding);
    }

    public static Rect CenterOn(this Rect rect, Vector2 center) {
        float h = rect.height / 2f;
        float w = rect.width / 2f;
        return new Rect(center.x - w, center.y - h, rect.width, rect.height);
    }

    public static Rect CenterOn(this Rect rect, float x, float y) {
        float h = rect.height / 2f;
        float w = rect.width / 2f;
        return new Rect(x - w, y - h, rect.width, rect.height);
    }

    public static Rect CenterOn(this Rect rect, Rect center) {
        float h = rect.height / 2f;
        float w = rect.width / 2f;
        return new Rect(center.x - w, center.y - h, rect.width, rect.height);
    }

    public static Rect ToScreen(this Rect rect) {

        float minY = Mathf.InverseLerp(Screen.height, 0, rect.yMax);
        float minX = Mathf.InverseLerp(0, Screen.width, rect.xMin);
        float maxY = Mathf.InverseLerp(Screen.height, 0, rect.yMin);
        float maxX = Mathf.InverseLerp(0, Screen.width, rect.xMax);
        return new Rect(minX, minY, maxX, maxY);

    }

    public static Rect ToCameraPixel(this Rect rect) {
        Rect r = new Rect(rect.x, Screen.height - rect.y - rect.height, rect.width, rect.height);
        return r;
    }

    public static void DebugRect(Rect rect, int padding = 0) {

        string a = rect.xMin + "," + rect.yMin;
        string b = rect.xMin + "," + rect.yMax;
        string c = rect.xMax + "," + rect.yMax;
        string d = rect.xMax + "," + rect.yMin;

        var s = GUI.skin.label;

        var wa = s.CalcSize(new GUIContent(a));
        var wb = s.CalcSize(new GUIContent(b));
        var wc = s.CalcSize(new GUIContent(c));
        var wd = s.CalcSize(new GUIContent(d));

        if (padding > 0)
            rect = rect.getWithPadding(padding);

        GUI.Label(new Rect(rect.xMin, rect.yMin, wa.x, wa.y), a);
        GUI.Label(new Rect(rect.xMin, rect.yMax - wb.y, wb.x, wb.y), b);
        GUI.Label(new Rect(rect.xMax - wc.x, rect.yMax - wc.y, wc.x, wc.y), c);
        GUI.Label(new Rect(rect.xMax - wd.x, rect.yMin, wd.x, wd.y), d);
    }

}

