﻿using System;
using System.Linq;

namespace ECS.Internal
{
    /// <summary>
    /// Tracks active components
    /// </summary>
    public class ComponentTrackerBase
    {
        /// <summary>
        /// Corresponding typeid of components stored
        /// </summary>
        protected readonly int typeid;

        /// <summary>
        /// Type of components stored.
        /// </summary>
        protected readonly Type type;

        /// <summary>
        /// How much the internal arrays will grow.
        /// </summary>
        protected int blockSize = ECSRoot.PoolBlockSize;

        /// <summary>
        /// Total current size of internal arrays
        /// </summary>
        protected int currentSize = -1;

        protected EntityManager manager;
        protected Matcher[] matchers;

        protected int count;
        public int Count => count;

        public ComponentTrackerBase(int typeid, Type type, EntityManager manager)
        {
            this.manager = manager;
            this.typeid = typeid;
            this.type = type;
            this.matchers = new Matcher[0];
        }

        public virtual Comp Add(Entity entity) => null;

        public virtual void AddExisting(Comp comp, Entity entity) { }

        public virtual void Destroy(Entity entity, bool skipEntityComponentArrayRemoval = false) { }

        public void RegisterMatcher(Matcher matcher)
        {
            if (this.matchers.Contains(matcher))
            {
                return;
            }

            var old = this.matchers;
            this.matchers = new Matcher[old.Length + 1];
            Array.Copy(old, this.matchers, old.Length);
            this.matchers[this.matchers.Length - 1] = matcher;
        }

        public virtual void RegisterSystemCompAdded(ECSSystem system) { }
        public virtual void RegisterSystemCompRemoved(ECSSystem system) { }

        public virtual Comp Get(int id) => null;
        public virtual Comp GetAny() => null;
    } 
}