﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ECS;
using UnityEngine;

public class TestComponent : MonoBehaviour
{
    public Collider2D target;
    public int id = 1;

    private ECSRoot root;

    // Use this for initialization
    void Start()
    {
        root = GameObject.FindObjectOfType<Game>().ECS;

        //st.Restart();
        //for (int i = 0; i < cycles; i++)
        //{
        //    BoxCollider2D box = (BoxCollider2D)GetComponent(typeof(BoxCollider2D));
        //}
        //UnityEngine.Debug.Log("GetComponents:" +st.ElapsedMilliseconds);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 100, 30), "Test"))
        {
            int cycles = 10000000;
            ComponentTracker<Position> tracker = (ComponentTracker<Position>)root.EntityManager.GetPool(Position.typeid);
            Stopwatch st = Stopwatch.StartNew();
            Entity e = root.EntityManager.GetEntity(target.EntityID);

            for (int i = 0; i < cycles; i++)
            {
                Position pos = tracker.GetGeneric(e.ID);
            }
            UnityEngine.Debug.Log("Get ID generic:" + st.ElapsedMilliseconds);

            st.Restart();
            for (int i = 0; i < cycles; i++)
            {
                Position pos = (Position)root.EntityManager.GetComponent(Position.typeid, e.ID);
            }
            UnityEngine.Debug.Log("Get ID upcast:" + st.ElapsedMilliseconds);
        }
    }
}
