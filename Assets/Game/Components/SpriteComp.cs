﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class SpriteComp : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public Sprite sprite;
    public Color color = new Color(1,1,1,1);

    public override void Reset()
    {
        sprite = null;
    }
}
