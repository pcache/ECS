﻿using System;
using System.Collections.Generic;
using ECS;

[System.Serializable]
public class GameRules : SingletonComp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public float timeBetweenSpawns;
    public float nextEnemyCountdown;
    public int maxEnemies = 1;
    public int enemyBatch = 1;

    public override void Reset()
    {
    }
}
