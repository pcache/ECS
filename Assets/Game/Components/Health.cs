﻿using System;
using System.Collections.Generic;
using ECS;

[System.Serializable]
public class Health : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public float health;

    public override void Reset()
    {
        health = 0;
    }
}
