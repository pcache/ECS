﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class Size : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public Vector2 size;

    public CollisionLayer layer;
    public enum CollisionLayer
    {
        Projectile = 8,
        Enemy = 9,
        Player = 10
    }

    public override void Reset()
    {
        size = new Vector2();
    }
}
