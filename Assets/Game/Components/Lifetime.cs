﻿using System;
using System.Collections.Generic;
using ECS;

[System.Serializable]
public class Lifetime : Comp
{
    public static readonly int typeid;
    public override int Typeid => typeid;

    public float lifetimeLeft;

    public override void Reset()
    {
        lifetimeLeft = 1;
    }
}
