﻿
using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class WorldGameObjectTemplate : GameObjectTemplate
{

    public SpriteRenderer spriteRenderer;

    public override GameObject Construct()
    {
        GameObject go = new GameObject();

        spriteRenderer = go.AddComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;

        collider = go.AddComponent<BoxCollider2D>();
        collider.enabled = false;

        return go;
    }

    public override void Reset()
    {
        spriteRenderer.color = Color.white;
    }
}
