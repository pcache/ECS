﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class WorldObjectSystem : ECSSystem,
    IComponentAddedReceiver<WorldObject>,
    IComponentRemovedReceiver<WorldObject>
{
    private GameObjectPool<WorldGameObjectTemplate> worldGOpool;

    [Inject] private ComponentTracker<SpriteComp> sprites;
    [Inject] private ComponentTracker<Position> positions;
    [Inject] private ComponentTracker<Size> sizes;
    [Inject] private ComponentTracker<WorldObject> wos;

    [Inject(typeof(WorldObject), typeof(Position))]
    private Match match;


    public WorldObjectSystem(ECSRoot root) : base(root)
    {
        this.worldGOpool = new GameObjectPool<WorldGameObjectTemplate>();
    }

    public void OnCompAdded(WorldObject comp, Entity entity)
    {
        SpriteComp sprite = sprites.ComponentsByEntity[entity.ID];

        var template = worldGOpool.Get(entity);
        comp.template = template;
        template.spriteRenderer.sprite = sprite.sprite;
        template.spriteRenderer.color = sprite.color;
        template.spriteRenderer.enabled = true;

        Size size = sizes.ComponentsByEntity[entity.ID];

        if (size != null)
        {
            template.gameObject.layer = (int)size.layer;
            template.collider.enabled = true;
            ((BoxCollider2D)template.collider).size = size.size;
        }

    }

    public void OnCompRemoved(WorldObject comp, Entity entity)
    {
        var template = comp.template;
        template.spriteRenderer.enabled = false;
        template.collider.enabled = false;
        worldGOpool.Release(template);
    }

    public override void Update(float delta)
    {
        for (int i = 0; i < match.Length; i++)
        {
            Entity e = entityManager.GetEntity(match.Entities[i]);

            WorldObject wo = wos.ComponentsByEntity[e.ID];
            Position pos = positions.ComponentsByEntity[e.ID];

            wo.template.transform.position = new Vector2(pos.x, pos.y);
            wo.template.transform.rotation = Quaternion.Euler(new Vector3(0, 0, pos.rotation));
        }
    }

    public override void OnDrawGizmos(float delta)
    {
        for (int i = 0; i < match.Length; i++)
        {
            Entity e = entityManager.GetEntity(match.Entities[i]);

            WorldObject wo = wos.ComponentsByEntity[e.ID];
            Position pos = positions.ComponentsByEntity[e.ID];

            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(pos.position, pos.position + pos.forward);

            Gizmos.color = Color.white;

        }
    }

    public override void OnGUI(float delta)
    {

    }
}
