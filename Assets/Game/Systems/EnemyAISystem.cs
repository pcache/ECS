﻿using System;
using System.Collections.Generic;
using ECS;

public class EnemyAISystem : ECSSystem, IComponentAddedReceiver<EnemyAI>
{
    [Inject] ComponentTracker<EnemyAI> ais;
    [Inject] ComponentTracker<Position> positions;
    [Inject] ComponentTracker<MoveForward> mfTracker;
    [Inject] SingletonTracker<Player> player;

    public EnemyAISystem(ECSRoot root) : base(root)
    {
    }

    public void OnCompAdded(EnemyAI comp, Entity entity)
    {
        comp.target = player.Component.EntityID;
    }

    public override void Update(float delta)
    {
        int count = ais.Count;
        for (int i = 0; i < count; i++)
        {
            EnemyAI ai = ais.Components[i];
            Entity e = entityManager.GetEntity(ai.EntityID);

            Position pos = positions[e.ID];
            Position playerPos = positions[ai.target];
            MoveForward mf = mfTracker[e.ID];

            pos.rotation = VectorUtils.DirectionToRotation(VectorUtils.GetDirection(pos.position, playerPos.position));

            if(ai.target > -1)
            {
                if(mf == null)
                {
                    mf = mfTracker.AddGeneric(e);
                    mf.speed = ai.speed;
                }
            }
            else
            {
                if(mf != null)
                {
                    mfTracker.Destroy(e);
                }
            }
        }
    }
}
