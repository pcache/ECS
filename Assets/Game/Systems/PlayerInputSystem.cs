﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class PlayerInputSystem : ECSSystem
{
    [Inject] SingletonTracker<PlayerInput> inputtracker;

    public PlayerInputSystem(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        PlayerInput input = inputtracker.Component;

        input.Horizontal = Input.GetAxisRaw("Horizontal");
        input.Vertical = Input.GetAxisRaw("Vertical");
        input.Shoot = new PlayerInput.KeyEvent(KeyCode.Mouse0);

        Vector2 mousepos = Input.mousePosition;
        input.mousePos = mousepos;

        Vector2 mouseposworld = Camera.main.ScreenToWorldPoint(mousepos);
        input.mousePosWorld = mouseposworld;
    }
}
