﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class SineMoverSystem : ECSSystem
{

    [Inject(typeof(SineMove), typeof(Position))]
    private Match match;

    [Inject]
    private ComponentTracker<Position> positions;

    [Inject]
    private ComponentTracker<SineMove> sinemovers;

    public SineMoverSystem(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        for (int i = 0; i < match.Length; i++)
        {
            Entity e = entityManager.GetEntity(match.Entities[i]);

            Position pos = positions[e.ID];
            SineMove sine = sinemovers[e.ID];

            sine.sineAccumulator += delta;

            pos.x = Mathf.Sin(sine.sineAccumulator * 10) * delta * 10;
            pos.rotation += delta;
        }
    }

    public override void OnDrawGizmos(float delta)
    {
        return;
        for (int i = 0; i < match.Length; i++)
        {
            Entity e = entityManager.GetEntity(match.Entities[i]);

            Position pos = positions[e.ID];

            Gizmos.DrawWireCube(new Vector3(pos.x, pos.y), new Vector3(1, 1, 1));

        }
    }
}
