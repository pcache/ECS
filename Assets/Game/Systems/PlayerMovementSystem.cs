﻿// Alexander Antonov, www.pointcache.com all rights reserved. 

using System;
using System.Collections.Generic;
using ECS;
using UnityEngine;

public class PlayerMovementSystem : ECSSystem
{

    [Inject] ComponentTracker<Position> positionTracker;

    private float rotation;

    public PlayerMovementSystem(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        Player player = (Player)entityManager.GetSingletonComponent(Player.typeid);
        Entity e = entityManager.GetEntity(player.EntityID);

        PlayerInput input = (PlayerInput)entityManager.GetSingletonComponent(PlayerInput.typeid);
        Position pos = positionTracker[e.ID];

        // TEMP

        pos.rotation = VectorUtils.LookAt(pos.position, input.mousePosWorld);

        pos.x += input.Horizontal * delta * player.speed;
        pos.y += input.Vertical * delta * player.speed;
        //pos.x += input.Horizontal * delta * player.speed;
        //pos.x = Mathf.Clamp(pos.x, -8, 8);

    }

    public override void OnGUI(float delta)
    {
        GUI.Label(new Rect(100, 0, 100, 30), "rotation:" + rotation);
    }
}
