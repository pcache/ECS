﻿using System;
using System.Collections.Generic;
using ECS;

public class EnemySpawningSystem : ECSSystem
{
    [Inject] SingletonTracker<GameRules> gamedataTracker;
    [Inject] ComponentTracker<EnemyAI> ais;



    public EnemySpawningSystem(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        GameRules data = gamedataTracker.Component;

        data.nextEnemyCountdown -= delta;

        if (ais.Count < data.maxEnemies && data.nextEnemyCountdown < 0)
        {
            for (int i = 0; i < data.enemyBatch; i++)
            {
                SpawnEnemy();
                data.nextEnemyCountdown = data.timeBetweenSpawns;
            }
        }
    }

    private void SpawnEnemy()
    {
        Entity e = entityManager.CreateEntity("enemy");

        Position pos = ComponentPool<Position>.Next;

        //Size size = ComponentPool<Size>.Next;
        //size.size = new UnityEngine.Vector2(2, 1);
        //size.layer = Size.CollisionLayer.Enemy;

        SpriteComp sprite = ComponentPool<SpriteComp>.Next;
        sprite.sprite = GameSprites.instance.enemy;

        EnemyAI enemy = ComponentPool<EnemyAI>.Next;
        enemy.timeBetweenShots = 3;
        enemy.speed = UnityEngine.Random.Range(0.1f, 3f);

        Health health = ComponentPool<Health>.Next;
        health.health = 1;

        e.AddComponent(pos);
        //e.AddComponent(size);
        e.AddComponent(enemy);
        e.AddComponent(health);
        e.AddComponent(sprite);
        e.AddComponent(WorldObject.typeid);
    }
}
