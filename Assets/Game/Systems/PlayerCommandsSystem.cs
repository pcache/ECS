﻿using System;
using System.Collections.Generic;
using ECS;

public class PlayerCommandsSystem : ECSSystem
{
    [Inject(typeof(Player), typeof(PlayerInput), typeof(Weapon))]
    private Match match;

    public PlayerCommandsSystem(ECSRoot root) : base(root)
    {
    }

    public override void Update(float delta)
    {
        if (match.Length > 0)
        {
            Entity e = entityManager.GetEntity(match.Entities[0]);

            PlayerInput input = (PlayerInput) entityManager.GetComponent(PlayerInput.typeid, e);

            if (input.Shoot.Down)
            {
                e.AddComponent(ShotCommand.typeid);
            }
        }
    }
}
