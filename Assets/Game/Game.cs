﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public class Game : ECSRootMono
{

    private GameSprites sprites;

    protected override void Start()
    {
        base.Start();
        sprites = GetComponent<GameSprites>();

        ECS.SystemsController.AddSystem<PlayerInputSystem>();
        ECS.SystemsController.AddSystem<PlayerMovementSystem>();
        ECS.SystemsController.AddSystem<PlayerCommandsSystem>();
        ECS.SystemsController.AddSystem<ShootingSystem>();
        ECS.SystemsController.AddSystem<MoveForwardSystem>();
        ECS.SystemsController.AddSystem<LifetimeSystem>();
        ECS.SystemsController.AddSystem<EnemySpawningSystem>();
        ECS.SystemsController.AddSystem<EnemyAISystem>();

        ECS.SystemsController.AddSystem<WorldObjectSystem>();

        SpawnGameData();
        SpawnPlayer();
    }

    protected override void Update()
    {
        base.Update();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ECS.Paused = !ECS.Paused;
        }
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
    }

    protected override void OnGUI()
    {
        base.OnGUI();
        GUI.Label(new Rect(0, 0, 100, 30), "Entities:" + this.ECS.EntityManager.Count.ToString());
    }

    private void SpawnGameData()
    {
        Entity e = ECS.EntityManager.CreateEntity("GameData");

        GameRules data = new GameRules();

        ECS.EntityManager.AddComponent(data, e);

    }

    private void SpawnPlayer()
    {
        Entity e = ECS.EntityManager.CreateEntity("Player");

        SpriteComp sprite = ComponentPool<SpriteComp>.Get();
        sprite.sprite = sprites.ship;

        Position pos = ComponentPool<Position>.Get();
        pos.y = -4f;

        Size size = ComponentPool<Size>.Get();
        size.size = new Vector2(1, 1);

        Player player = new Player()
        {
            ammo = 100,
            speed = 5
        };

        Health health = new Health()
        {
            health = 10
        };

        Weapon weapon = ComponentPool<Weapon>.Get();
        weapon.damage = 1;
        weapon.projectile = Weapon.Projectile.player;
        weapon.speed = 15;

        e.AddComponent(sprite);
        e.AddComponent(pos);
        e.AddComponent(size);
        e.AddComponent(PlayerInput.typeid);
        e.AddComponent(player);
        e.AddComponent(health);
        e.AddComponent(weapon);

        e.AddComponent(WorldObject.typeid);
    }

}

